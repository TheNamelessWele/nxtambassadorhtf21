using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SettingsMenu : MonoBehaviour
{
    //reference audio section
    public AudioMixer audioMixer;
    public void SetMusicVolume(float volume)
    {
        audioMixer.SetFloat("MusicParam", volume);
    }

    public void SetSpeakingVolume(float volume)
    {
        audioMixer.SetFloat("SpeakingParam", volume);
    }

    public void SetBackgroundVolume(float volume)
    {
        audioMixer.SetFloat("BackgroundParam", volume);
    }


    //resume game
}
