using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecipeScript : MonoBehaviour
{
    //if an ingredient hits the bottom of the pan

    public int ingredientAmount = 0;
    public GameObject pot;
    public GameObject medicine;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ingredient")
        {
            other.gameObject.SetActive(false);
            float position = gameObject.transform.position.y + 0.05f;
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, position, gameObject.transform.position.z);
            ingredientAmount++;
        }
        else if(other.tag == "Spoon")
        {
            if(ingredientAmount == 3)
            {
                other.gameObject.SetActive(false);
                pot.SetActive(false);
                medicine.SetActive(true);
            }
        }
        
    }
}
