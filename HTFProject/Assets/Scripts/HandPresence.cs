using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class HandPresence : MonoBehaviour
{
    public GameObject handModelPrefab;
    public InputDeviceCharacteristics controllerChar;

    private InputDevice targetDevice;
    private GameObject spawnedHandModel;
    private Animator handAnimator = null;
    private List<InputDevice> devices = new List<InputDevice>();

    // Start is called before the first frame update
    void Start()
    {
        InputDevices.GetDevicesWithCharacteristics(controllerChar, devices);
        Debug.Log("logging input devices");
        Debug.Log(devices.Count);

        foreach (InputDevice item in devices)
        {
            Debug.Log(item.name + item.characteristics);
        }

        if (devices.Count > 0)
        {
            targetDevice = devices[0];

            spawnedHandModel = Instantiate(handModelPrefab, transform);
            handAnimator = spawnedHandModel.GetComponent<Animator>();
        }
    }

    void UpdateAnimation()
    {
        if (targetDevice.TryGetFeatureValue(CommonUsages.trigger, out float triggerVal))
        {
            handAnimator.SetFloat("trigger", triggerVal);
        }
        else
        {
            handAnimator.SetFloat("trigger", 0);
        }

        if (targetDevice.TryGetFeatureValue(CommonUsages.grip, out float gripVal))
        {
            handAnimator.SetFloat("grip", gripVal);
        }
        else
        {
            handAnimator.SetFloat("grip", 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (devices.Count < 1)
        {
            InputDevices.GetDevicesWithCharacteristics(controllerChar, devices);
        }
        if (handAnimator != null)
        {
            targetDevice.TryGetFeatureValue(CommonUsages.trigger, out float triggerVal);
            Debug.Log("Feature value Trigger: " + triggerVal);
            Debug.Log("Hand model: " + spawnedHandModel);
            Debug.Log("Hand Presence Trigger: " + handAnimator.GetParameter(0).name);
            UpdateAnimation();
        }
    }
}
