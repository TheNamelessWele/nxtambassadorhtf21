using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerScript : MonoBehaviour
{
    //to do: 
    //if object with tag Grabbable collides with player, and the object velocity is (much) higher than the velocity of the player, player takes (some) damage 
    //if NPC has the attack state, and NPC is within (punching distance) from the player, player takes damage (and is pushed back a little?)
    public NPCScript npcScript;
    public int playerHealth = 50;
    public int currentHealth;
    protected Rigidbody playerRb;
    protected bool attacking = false;
    public GameObject npc;
    public GameObject bloodSplat;
    public GameObject bloodSplat1;
    public GameObject bloodSplat2;
    public GameObject bloodSplat3;
    public GameObject bloodSplat4;


    // Start is called before the first frame update
    void Start()
    {
        playerRb = this.gameObject.GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.LogError("In the OnCollisionEnter!" + collision.gameObject.tag);
        if (collision.gameObject.tag == "Grabbable" && collision.gameObject.GetComponent<Rigidbody>().velocity.x > 1.2 * playerRb.velocity.x
            || collision.gameObject.tag == "Grabbable" && collision.gameObject.GetComponent<Rigidbody>().velocity.y > 1.2 * playerRb.velocity.y
            || collision.gameObject.tag == "Grabbable" && collision.gameObject.GetComponent<Rigidbody>().velocity.z > 1.2 * playerRb.velocity.z)
        //if the object colliding with the player is going faster than 1.2 times the speed of the player
        {
            //player takes damage
            TakeDamage(5);
        }
        if(collision.gameObject.tag == "Hand")
        {
            Debug.LogError("Ow! Stop hitting me!");
            if(npc.GetComponent<NPCScript>().aggroLevel > 4)
            {
                NPCAttack();
            } 
        }
        if(collision.gameObject.tag == "Ground")
        {
            SceneManager.LoadScene("Street");
        }
    }
    // Update is called once per frame
    void Update()
    {
        if(currentHealth < 50 && currentHealth > 40)
        {
            Debug.LogError("Tis but a scratch! - From: Health < 50");
            bloodSplat.SetActive(true);
            bloodSplat1.SetActive(false);
            bloodSplat2.SetActive(false);
            bloodSplat3.SetActive(false);
            bloodSplat4.SetActive(false);
        }
        else if (currentHealth <= 40 && currentHealth > 30)
        {
            Debug.LogError("Oh no! You've got an owie! - From: Health < 40");
            //is a bit bloody: sprite
            bloodSplat.SetActive(false);
            bloodSplat1.SetActive(true);
            bloodSplat2.SetActive(false);
            bloodSplat3.SetActive(false);
            bloodSplat4.SetActive(false);
        }
        else if (currentHealth <= 30 && currentHealth > 20)
        {
            Debug.LogError("...that looks like a nasty cut. - From: Health < 30");
            //is bloodied : sprite
            bloodSplat.SetActive(false);
            bloodSplat1.SetActive(false);
            bloodSplat2.SetActive(true);
            bloodSplat3.SetActive(false);
            bloodSplat4.SetActive(false);
        }
        else if (currentHealth <= 20 && currentHealth > 10)
        {
            Debug.LogError("Holy shit. Shall I call a doctor? - From: Health < 20");
            //is very bloodied: sprite
            bloodSplat.SetActive(false);
            bloodSplat1.SetActive(false);
            bloodSplat2.SetActive(false);
            bloodSplat3.SetActive(true);
            bloodSplat4.SetActive(false);
        }
        else if (currentHealth <= 10 && currentHealth > 0)
        {
            Debug.LogError("SOMEONE CALL AN AMBULANCE!!! - From: Health < 10");
            //end situation, be thrown onto street with You Lose screen or something. Ominous music?
            bloodSplat.SetActive(false);
            bloodSplat1.SetActive(false);
            bloodSplat2.SetActive(false);
            bloodSplat3.SetActive(false);
            bloodSplat4.SetActive(true);
        }
        else if(currentHealth == 0)
        {
            Debug.LogError("RIP - From: Health == 0");
            SceneManager.LoadScene("Street");
            //respawn in street scene, next to bed
        }
    }

    void TakeDamage(int damage)
    {
        if(currentHealth != 0)
        {
            currentHealth -= damage;
        }
        
    }

    public void NPCAttack()
    {
            TakeDamage(5);      
    }

    public bool HittingDistance()
    {
        if ((npc.gameObject.transform.position - this.gameObject.transform.position).sqrMagnitude < 5.0f)
        {
            Debug.LogError("NPC hits!" + (npc.gameObject.transform.position - this.gameObject.transform.position).sqrMagnitude.ToString());
            return true;
        }
        else
        {
            Debug.LogError("NPC does not hit" + (npc.gameObject.transform.position - this.gameObject.transform.position).sqrMagnitude.ToString());
            return false;
        }
    }
}
