using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabBehaviour : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        NpcNavigation npc = animator.GetComponent<NpcNavigation>();
        npc.target.transform.parent = npc.hand.transform;

        Rigidbody rb = npc.target.GetComponent<Rigidbody>();
        rb.isKinematic = true;
        npc.target.transform.localPosition = Vector3.zero;
        npc.target.transform.localRotation = Quaternion.Euler(0,0,0);

        animator.SetBool("HasNearbyObjects", false);
        npc.target = npc.player;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("hasObject", true);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
