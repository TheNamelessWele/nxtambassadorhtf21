using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using System.Threading;

/* To do:
 * - Make sure dialogue continues after several seconds (temporarily solved)
 * - See that everything stops (temporarily solved)
 * - Add IGrabbable to bottle, and isTrigger + script. If bottle collides with other.tag("npc"), generate dialogue "Dank u wel!" 
 */

[System.Serializable]
public class PrimaryButtonEvent : UnityEvent<bool> { }
public class Player : MonoBehaviour
{
    //for triggering interactions
    private GameObject triggeringNpc; //the NPC with the trigger collider
    private bool isTriggered = true;

    //dialogue
    public Button interactButton;
    public GameObject dialogueBox; //dialogue of NPC
    private DialogueTrigger dialogueTrigger = new DialogueTrigger();

    //For the controller
    public XRController controller;
    UnityEngine.XR.InputDevice device;

    void Start()
    {
    }
    void Update()
    {
        
    }
    
    public void TaskOnClick()
    {
        Debug.LogError("Button has been clicked!");

    }

    public void StartingDialogue()
    {
            dialogueBox.SetActive(true);
            Destroy(dialogueBox, 10);    //Temporary solution.
            //dialogueBox.SetActive(false);
            dialogueTrigger.TriggerDialogue();
    }
    public void EndDialogue()
    {
        Debug.LogError("End of conversation");
        dialogueBox.SetActive(false);
    }

    //for when the player gets near the NPC
    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Npc")
        {
            isTriggered = true;
            triggeringNpc = other.gameObject;
        }

        if (other.tag == "Player")
        {
            Debug.LogError("Fuck."); //testing whether it's the player that triggers, and not the NPC. Turns out it's just broken. 
        }
    }

    void OnTriggerExit(Collider other)
    {
            isTriggered = false;
        triggeringNpc = null;
    }

}
