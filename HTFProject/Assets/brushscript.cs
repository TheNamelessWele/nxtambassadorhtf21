using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class brushscript : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject fingerPrints;
    public GameObject murderPic;
    public GameObject knifePic;
    public GameObject dna;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "brush")
        {
            fingerPrints.SetActive(true);
        }
        if(other.tag == "camera" && this.tag == "body")
        {
            murderPic.SetActive(true);
        }
        if (other.tag == "camera" && this.tag == "knife")
        {
            knifePic.SetActive(true);
        }
        if(other.tag == "swab")
        {
            dna.SetActive(true);
        }
    }
}
