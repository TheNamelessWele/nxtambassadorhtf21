using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PaceBehaviour : StateMachineBehaviour
{
    GameObject player;
    Coroutine pace;
    NavMeshAgent agent;
    bool isPos1 = true;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //pace = animator.GetComponent<NpcNavigation>().StartCoroutine("Pace");
        agent = animator.GetComponent<NavMeshAgent>();
        player = animator.GetComponent<NpcNavigation>().player;
        //NavMeshAgent agent = animator.GetComponent<NavMeshAgent>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        NavMesh.SamplePosition(animator.gameObject.transform.position - player.transform.TransformDirection(Vector3.right)*3, out NavMeshHit hit1, 2, -1);
        NavMesh.SamplePosition(animator.gameObject.transform.position + player.transform.TransformDirection(Vector3.right)*3, out NavMeshHit hit2, 2, -1);
        if (isPos1 && !agent.hasPath)
        {
            Debug.LogError("path 1");
            agent.SetDestination(hit1.position);
        }
        else if (!isPos1 && !agent.hasPath)
        {
            Debug.LogError("path 2");
            agent.SetDestination(hit2.position);
        }
        if (!agent.hasPath) isPos1 = !isPos1;
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //animator.GetComponent<NpcNavigation>().StopCoroutine(pace);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
