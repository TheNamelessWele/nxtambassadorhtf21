using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour
{

    public void PlayNewGame()
    {
        //create new save file
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); //loads the next active scene in the row.
    }

    public void ContinueGame()
    {
        //if Continue || Hervatten is clicked, check if there's a saved game. 
        //If so, continue from saved game. If not, create new save file. 
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame()
    {
        Debug.Log("QUIT!"); //temporary, to show that it can quit. 
        Application.Quit();
    }
}
