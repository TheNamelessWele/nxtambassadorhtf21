using System.Collections; 
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Tutorial : MonoBehaviour
{  //things to do to make the medicine: 
   //open menu ("Welkom bij het menu")
   // explain which items are where (Next button)
   //      "U kunt hier kiezen voor verbale acties" - highlight Verbaal 
   //      "Terug gaan naar het menu" 
   //      "Of op 'Help' drukken als u een korte uitleg van alles wilt"
   // explain how to teleport 
   //      "A) Druk op de knop bij uw wijsvinger. B) Mik naar waar u wilt teleporteren. C) Laat los
   // explain how to pick up objects 
   //      "Iet oppakken doet u met de knop van uw middelvinger" 
   // player needs to grab all the bottles and put them in the pan (doortutorial again?)
   // player needs to go to Dialogue part of menu, and utter a magic word 
   // player needs to stir the recipe with a spoon 


    //to open the doors
    public GameObject door1;
    public GameObject door2;

    public GameObject outside;

    public GameObject doorTutorial;
    public TMPro.TextMeshProUGUI tutorialText1; //the text on the door
    public TMPro.TextMeshProUGUI tutorialText2; //the text on the second tutorialObject
    public GameObject tutorialMenu; //the tutorial part of the player Menu
    public GameObject tutorialObject; //the first tutorial object, on the door
    public GameObject tutorialObject2; //the second tutorial object, on the wall

    public GameObject player;
    private Vector3 currentPos;
    private Vector3 previousPos;
    protected bool objectWasGrabbed = false;
    public GameObject medicine;
    public GameObject liquid;

    public float timer = 5f;
    private bool menuState = false;
    public GameObject teleportCircle;

    //to start the tutorial: change text on the "board" first, then create menu options

    //klik de Oculus Home button op je linker controller om het menu open te doen
    //if menu is open, tutorial change text disappears 

    public void StartTutorial() //starts the tutorial. 
    {
        if(tutorialMenu.activeSelf == true) //if the menu is open (player clicked on the right button)
        {
            menuState = true;
        }
        
    }
    public void ContinueTutorial() //if back button is pressed in menu
    {
        teleportCircle.SetActive(true);
        tutorialText1.text = "Teleporteer naar de gouden cirkel";
    }
    public void ChangeColor() //changes the color of the stuff in the pot, if you use the spell
    {
        var liquidColor = liquid.GetComponent<Renderer>();
        liquidColor.material.SetColor("_Color", Color.green);
    }
    public void GrabbedObject() //indicates that the player has indeed managed to grab an object
    {
        objectWasGrabbed = true;
    }


    // Start is called before the first frame update
    void Start()
    {
        previousPos = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z); //logs the first location of the player
    }

    // Update is called once per frame
    void Update()
    {
        currentPos = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
        StartTutorial();

        if (currentPos != previousPos) //if the player has managed to move, the current position is not the same as the previous one
        {
            teleportCircle.SetActive(false);
            tutorialObject.SetActive(false);
            tutorialObject2.SetActive(true);
            tutorialText2.text = "Heel goed! Pak nu de lepel op.";
            if (objectWasGrabbed == true) //if the player has managed to grab an object
            {
                tutorialText2.text = "Fantastisch! Volg nu het recept, en dan ben je er klaar voor.";
            }
        }
        if (medicine.activeSelf == true)
        {
            tutorialMenu.SetActive(false);
            OpenDoors();
            timer = 5;
        }
    }

    public void OpenDoors()
    {
        door1.transform.position = new Vector3(door1.transform.position.y - 1.365f, door1.transform.position.x, door1.transform.position.z);
        door2.transform.position = new Vector3(door2.transform.position.y + 1.365f, door2.transform.position.x, door2.transform.position.z);
        tutorialMenu.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            tutorialObject.SetActive(false);
        }
    }

}
