using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectVisibilityToggler : MonoBehaviour
{
    public void Start()
    {
        
    }
    public void ToggleObject()
    {
        this.gameObject.SetActive(!this.gameObject.activeSelf);
    }
}
