using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NonRectangularButtonScript : MonoBehaviour
{
    public float alphaTreshold = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Image>().alphaHitTestMinimumThreshold = alphaTreshold;
    }
}
