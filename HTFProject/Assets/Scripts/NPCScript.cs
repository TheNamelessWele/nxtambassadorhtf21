using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCScript : MonoBehaviour
{
    public string npcName; //name of NPC
    public Text npcNameText;
    public int startAggroLevel; //level of aggression to start off with
    public int aggroLevel; //level of aggression at the moment
    public float aggroRatio; //how fast the NPC becomes more angry: Values between 1.25 and 1.75
    public float calmRatio; //how fast the NPC calms down: Values between 1.25 and 1.75
    private bool dialogueTrigger = false;
    private GameObject dialogueBox;
    private Text text;
    private Animator animator;

    public NPCScript(string npcName, int aggroLevel, float aggroRatio, float calmRatio)
    {
        this.npcName = npcName;
        this.aggroLevel = aggroLevel;
        this.aggroRatio = aggroRatio;
        this.calmRatio = calmRatio;
    }

    //OldScript
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && dialogueTrigger == false)
        {
            dialogueTrigger = true;
            FindObjectOfType<DialogueScript>().StartSituation(); //starts the situation  
        }
        else
        {
            //do nothing
        }
    }

    void OnTriggerStay(Collider other) //checks whether the player is with NPC range, just in case the collider didn't pick it up the first time
    {
        if (other.tag == "Player" && dialogueTrigger == false) //checks whether the dialoguebox is already open
        {
            dialogueTrigger = true;
            FindObjectOfType<DialogueScript>().StartSituation();
            //dialogue script
        }
        else
        {
            //do nothing
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && dialogueTrigger == true)
        {
            dialogueTrigger = false;
            FindObjectOfType<DialogueScript>().EndDialogue();
        }
    }


    // Start is called before the first frame update
    private void Start()
    {
        npcNameText.text = npcName;  
    }

    // Update is called once per frame
    void Update()
    {
        if (animator == null)
        {
            animator = GetComponent<Animator>();
        }
        animator.SetInteger("AggroLevel", aggroLevel); //pass the aggro level over to the animator to affect the state
    }
}
