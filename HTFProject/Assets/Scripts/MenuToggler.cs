using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using System;

public class MenuToggler : MonoBehaviour
{
    public InputActionReference menuButton;

    public UnityEvent onMenuOpen;
    public UnityEvent onMenuClose;

    private bool isMenuOpen;

    void Start()
    {
        menuButton.action.performed += MenuToggle;
    }

    private void MenuToggle(InputAction.CallbackContext obj)
    {
        if (isMenuOpen)
        {
            isMenuOpen = false;
            onMenuClose.Invoke();
        }
        else
        {
            isMenuOpen = true;
            onMenuOpen.Invoke();
        }
    }
    public void CloseMenu()
    {
        isMenuOpen = false;
        onMenuClose.Invoke();
    }
}
