using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChange : MonoBehaviour
{
    public GameObject player;
    public GameObject spawnPoint;

    public void MainSceneStart()
    {
        Debug.LogError("It works!");
        SceneManager.LoadScene("Scene1");
    }

    public void BackToStreet()
    {
        SceneManager.LoadScene("Street");
        //for later: spawn in the same location as you left
    }

    public void DeathSpawn()
    {
        SceneManager.LoadScene("Street");
        player.transform.position = spawnPoint.transform.position;
        player.transform.rotation = spawnPoint.transform.rotation;
    }

    public void ExitGame()
    {
        Debug.LogError("Succesfully quit application");
        Application.Quit();
        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
